const user = {"name": "Mannat", "age": 20}

//Can manipulate object properties
user.name = "Muskan";

console.log(user);

//Cannot re-assign the entire object
//user = {"name": "aditya", "age": "20"}  