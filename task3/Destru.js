// Destructuring in Array
[std1, std2 , std3 , std4] = ['Mayank','Vishal','Aditya','keyur'];  
console.log(std1);   
console.log(std2); 
console.log(std3); 
console.log(std4);



//Destructuring in objects 

const person = {
    name: 'Muskan soni',
    age: 21,
    profession: {
        Department:'IT',
        Designing: 'web-designing'
        
    }
};


const { name,age, profession: {Designing, Department} } = person;

console.log(`${name} Here: profession is ${Designing} in ${Department} field,and I m ${age} year old.`);
